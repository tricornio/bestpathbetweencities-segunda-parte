# Fundamentos de los Sistemas Inteligentes #

### Práctica 1 - Segunda parte ###

Leer el PDF del repositorio para ver la descripción de la práctica.

### Tareas ###

* A partir del código base entregado, se deberá programar el método de ramificación y acotación con subestimacion. Utilícese como problema el grafo de las ciudades de Rumanía presente en el código.
* Comparar la cantidad de nodos expandidos por este método con relación a los métodos de búsqueda primero en anchura y primero en profundidad.
* Realizar a mano la traza de una búsqueda.