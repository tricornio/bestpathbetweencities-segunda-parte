# -*- coding: utf-8 -*-

# Search methods
import search

## Sibiu to Lugoj  ##
sl = search.GPSProblem('S', 'L', search.romania)

## Arad to Bucharest ##
# Ramificacion y acotacion con subestimacion: 15 (5 expansiones)
# FIFO: 59 (21 expansiones)
# STACK: 23 (tras quitar la lista cerrada hace un bucle infinito)
ab = search.GPSProblem('A', 'B', search.romania)

## Oradea to Giurgiu ##
# Ramificacion y acotacion con subestimacion: 26 (9 expansiones)
# FIFO: 148 (54 expansiones)
# STACK: 31 (tras quitar la lista cerrada hace un bucle infinito)
og = search.GPSProblem('O', 'G', search.romania)

## Craiova to Fagaras ##
# Ramificacion y acotacion con subestimacion: 21 (7 expansiones)
# FIFO: 61 (21 expansiones)
# STACK: 22 (tras quitar la lista cerrada hace un bucle infinito)
cf = search.GPSProblem('C', 'F', search.romania)

#print search.breadth_first_graph_search(ab).path() # FIFO (primero en anchura)
#print search.depth_first_graph_search(ab).path() # STACK (primero en profundidad)
#print search.iterative_deepening_search(ab).path()
#print search.depth_limited_search(ab).path()

"""
print ""
print " --------------------------------------------- "
print " ------------------> Traza <------------------ "
print " --------------------------------------------- "
print ""

#result = search.busqueda_ramificacion_acotacion(ab).path()[::-1]
#print search.breadth_first_graph_search(ab).path() # FIFO (primero en anchura)
#print search.depth_first_graph_search(ab).path() # STACK (primero en profundidad)


print ""
print " --------------------------------------------- "
print " ----------------> Resultado <---------------- "
print " --------------------------------------------- "
print ""

print "Para ir del " + str(result[0]) + " al " + str(result[len(result)-1]) + " el mejor camino es:"
print ""
print str(result) + " : " + str(result[len(result)-1].path_cost) + "KM"
"""


result = [0] * 20

result[0] = search.busqueda_ramificacion_acotacion_uno(ab)
result[1] = search.busqueda_ramificacion_acotacion_uno(cf)
result[2] = search.busqueda_ramificacion_acotacion_uno(sl)
result[3] = search.busqueda_ramificacion_acotacion_uno(og)

result[4] = search.busqueda_ramificacion_acotacion(ab)
result[5] = search.busqueda_ramificacion_acotacion(cf)
result[6] = search.busqueda_ramificacion_acotacion(sl)
result[7] = search.busqueda_ramificacion_acotacion(og)

result[8] = search.breadth_first_graph_search(ab)
result[9] = search.breadth_first_graph_search(cf)
result[10] = search.breadth_first_graph_search(sl)
result[11] = search.breadth_first_graph_search(og)

result[12] = search.busqueda_stack(ab)
result[13] = search.busqueda_stack(cf)
result[14] = search.busqueda_stack(sl)
result[15] = search.busqueda_stack(og)

print ""
print " --------------------------------------------- "
print " ------------------> TABLA <------------------ "
print " --------------------------------------------- "
print ""

print "                                                     | Arad to Bucharest |    | Craiova to Fagaras |      | Sibiu to Lugoj |      | Oradea to Giurgiu |"
print ""
print "Ramificacion y acotacion:"+ "                                   " +str(result[0]) + "                           " +str(result[1]) + "                       " +str(result[2]) + "                         " +str(result[3])
print ""
print "Ramificacion y acotacion con subestimacion:" + "                  " +str(result[4]) + "                            " +str(result[5]) + "                       " +str(result[6]) + "                           " +str(result[7])
print ""
print "Primero en anchura (FIFO):" + "                                  " +str(result[8]) + "                           " +str(result[9]) + "                       " +str(result[10]) + "                          " +str(result[11])
print ""
print "Primero en profundidad (STACK):"+ "                             " +str(result[12]) + "                            " +str(result[13]) + "                       " +str(result[14]) + "                          " +str(result[15])



# Result:
# [<Node B>, <Node P>, <Node R>, <Node S>, <Node A>] : 101 + 97 + 80 + 140 = 418
# [<Node B>, <Node F>, <Node S>, <Node A>] : 211 + 99 + 140 = 450

# Informacion del algoritmo: "Lectura 6 - Búsqueda informada pag. 59"
# Tener en cuenta que al expandir se puede volver al nodo padre desde el hijo, asi que cambia un poco respecto al ejercicio

# Realizado por Pablo José Díaz García y Dilma Sae
